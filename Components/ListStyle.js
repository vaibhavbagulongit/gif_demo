import React, {Component} from 'react';
import {Dimensions} from 'react-native';

const deviceHeight = Dimensions.get('screen').height;
const styles = {
  parentContainer: {
    height: deviceHeight,
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 10,
  },
  Layoutview: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  container: {
    backgroundColor: '#fff',
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  list: {
    paddingVertical: 4,
    margin: 5,
    backgroundColor: '#fff',
  },
  gif: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
    width: 350,
  },

  card: {
    paddingVertical: 4,
    margin: 10,
    backgroundColor: '#fff',
  },
  dividerstyle: {
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
  },
  viewstyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 10,
  },
  welcomeContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  welcomeText: {
    fontSize: 25,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  mindbowser: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontStyle: 'italic',
    marginBottom: 20,
  },
  innterContainer: {
    margin: 18,
  },
};
export default styles;
