/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ApiContainerAPI from './Components/APIContainer';
import ListDetailed from './Components/ListDetailed';
import welcome from './Components/welcome';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerTitleAlign: 'center'}}>
        <Stack.Screen name="Welcome" component={welcome} />
        <Stack.Screen name="Gif List" component={ApiContainerAPI} />
        <Stack.Screen name="Gif Details" component={ListDetailed} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
